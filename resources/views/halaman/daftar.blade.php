@extends('layout.master')
@section('judul')
    Buat Acount Baru!
@endsection

@section('isi')
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf 
        <label>First name</label> <br><br>
        <input type="text" name="nama"> <br><br>
        <label>Last name</label> <br><br>
        <input type="text" name="nama"> <br><br>
        <label>Gender</label> <br><br>
        <input type="radio" name="gender">Male <br><br>
        <input type="radio" name="gender">Female <br><br>
        <input type="radio" name="gender">Other <br><br>

        <label>Nationality</label> <br><br>
        <select name="N">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Vietnam">Vietnam</option>
            <option value="Thailand">Thailand</option>
        </select> <br><br>

        <label>Language Spoken</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br><br>
        <input type="checkbox"> English <br><br>
        <input type="checkbox"> Other <br><br>

        <label>Bio</label> <br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection